const express = require('express');
const controllers = require('../controllers/userControllers');

const router = express.Router();
router.get('/me', controllers.getUser);
router.delete('/me' ,controllers.deleteUser);
router.patch('/me', controllers.changeUserPassword);

module.exports = router;