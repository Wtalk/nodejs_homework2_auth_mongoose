const formRegister = document.getElementById('reg-form');
const fromLogin = document.getElementById('login');

const register = async (event) => {
    event.preventDefault();

    const username = document.getElementById('username-register').value;
    const password = document.getElementById('password-register').value;

    const response = await fetch('/api/auth/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username,
            password
        })
    }).then(res => res.json())

    console.log('Response', response)
}

formRegister.addEventListener('submit', register);


const login = async (event) => {
    event.preventDefault();

    const username = document.getElementById('username-login').value;
    const password = document.getElementById('password-login').value;

    const response = await fetch('/api/auth/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username,
            password
        })
    }).then(res => res.json())

    if (response.status === 'ok') {
        console.log('Got the token: ', response.data)
    } else {
        alert(response.error)
    }

}

fromLogin.addEventListener('submit', login);