const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const notesRoutes = require('./routes/notesRoutes');

const config = require("./default.json")

const port = process.env.PORT || config.serverPort;

mongoose.connect('mongodb://localhost:27017/auth-database', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const app = express();
app.use(express.json());

app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/notes' , notesRoutes)

app.use('/', express.static(path.join(__dirname, 'src')));

app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});