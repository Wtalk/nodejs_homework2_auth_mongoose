const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema({
    id: {type: Schema.Types.ObjectId, required: true},
    completed: {type: Boolean, required: false, default: false},
    text: {type: String, required: true,},
    createdDate: {type: Date}
});

model = mongoose.model('NoteSchema', NoteSchema);
module.exports = model;
