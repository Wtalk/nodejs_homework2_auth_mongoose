const bcrypt = require('bcryptjs');
const config = require('../default.json');
const User = require('../models/user');
const jwt = require("jsonwebtoken");

const getToken = (req) => req.headers.authorization.split(' ')[1];

const getUser = async (req, res) => {
    const token = getToken(req);

    try {
        const userData = jwt.verify(token, config.secretKey);
        if (userData) {
            return res.status(200).json({
                user: {
                    _id: userData.id,
                    username: userData.username,
                    createdDate: userData.createdDate
                }
            })
        } else {
            return res.status(500).json({message: 'Internal server error'})
        }

    } catch (e) {
        return res.status(400).json({message: 'Bad request'})
    }
}

const deleteUser = async (req, res) => {
    const token = getToken(req);

    try {
        const user = jwt.verify(token, config.secretKey);
        const deleted = await User.findById(user.id).deleteOne()
        if (deleted.deletedCount === 1) {
            return res.status(200).json({message: "Success"})
        } else {
            return res.status(400).json({message: 'Bad request'})
        }

    } catch (e) {
        return res.status(500).json({message: 'Internal server error'})
    }
}

const changeUserPassword = async (req, res) => {
    const token = getToken(req);

    try {
        const userDataFromToken = jwt.verify(token, config.secretKey);
        const {oldPassword, newPassword} = req.body;
        if(!oldPassword || !newPassword) {
            return res.status(400).json({
                message: "Bad request"
            })
        }
        const user = await User.findOne({username: userDataFromToken.username});

        if(!user) {
            return res.status(400).json({
                message: 'Bad request'
            })
        }
        const isSame = await bcrypt.compare(oldPassword, user.password);
        if(!isSame) {
            return res.status(400).json({
                message: 'Bad request'
            })
        }
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();

        return res.status(200).json({
            message: "Success",
        });


    } catch (e) {
        return res.status(500).json({
            message: 'Internal server error'
        })
    }
}

module.exports = {
    getUser,
    deleteUser,
    changeUserPassword
}