const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require('../default.json')
const User = require('../models/user');


const register = async (req, res) => {
    const {username, password: plainPassword} = req.body;

    if (!username || !plainPassword) {
        return res.status(400).json({message: 'Some params are not specified'});
    }

    const password = await bcrypt.hash(plainPassword, 2);
    const createdDate = new Date().toISOString()

    try {
        const response = await User.create({
            username,
            password,
            createdDate
        })
        console.log('User created successfully: ', response);
        return res.status(200).json({message: 'Success'});
    } catch (error) {
        return res.status(500).json({message: 'Such username already exists'});
    }
}

const login = async (req, res) => {
    const {username, password} = req.body;

    const user = await User.findOne({username}).lean()

    if (!user) {
        return res.status(400).json({message: 'Bad request'})
    }
    if (await bcrypt.compare(password, user.password)) {
        const token = jwt.sign(
            {
                id: user._id,
                username: user.username,
                createdDate: user.createdDate
            },
            config.secretKey
        )
        return res.status(200).json({
            message: 'Success',
            jwt_token: token
        })
    }

    return res.status(500).json({message: 'Internal server error'})
}


module.exports = {
    register,
    login
}