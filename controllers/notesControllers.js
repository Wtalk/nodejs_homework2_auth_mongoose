const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Note = require('../models/note');
const jwt = require("jsonwebtoken");
const config = require("../default.json");

const getToken = (req) => req.headers.authorization.split(' ')[1];

const createNote = async (req, res) => {
    try {
        const token = getToken(req);
        const user = jwt.verify(token, config.secretKey);
        const {text} = req.body;

        const note = new Note({
            id: new ObjectId(user.id),
            text
        });
        await note.save();

        if (!note) {
            res.status(500).json({message: 'Internal server error'})
        }
        res.status(200).json({message: 'Success'})
    } catch (error) {
        res.status(400).json({message: 'Bad request'})
    }
}

module.exports = {
    createNote
}